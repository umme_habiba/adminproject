
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php "include_once('partials/head.php');" ?>
</head>
<body class="sb-nav-fixed">
<?php 
include_once('partials/head.php'); 
include_once('partials/header.php'); 
include_once('partials/sidebar.php');
?>
<div id="layoutSidenav_content">
<main>
    <?php
include_once('partials/content.php');?>
<div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                  Order Table
                </div>
                    <?php
                       include_once('partials/orderTable.php');
                    ?>
</div>
<?php
include_once('partials/footer.php');
?>
</main>
</div>



</body>
</html>