<?php
include_once('db/conn.php');
if(isset($_POST['insert'])){
$ProductTitle= $_POST['ProductTitle'];
$Price= $_POST['Price'];
$ProductsSpecification= $_POST['ProductsSpecification'];
$ProductDetails= $_POST['ProductDetails'];
$picture=$_FILES['image']?? '';
$image= $picture['name'];

$sql = "INSERT INTO `product` (`ProductId`, `ProductTitle`, `Price`, `ProductsSpecification`, `ProductDetails`, `image`,`created_at`, `updated_at`) VALUES (NULL, '$ProductTitle', '$Price', '$ProductsSpecification', '$ProductDetails', '$image',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}
$temp = $picture['tmp_name']?? '';
$destination=$_SERVER['DOCUMENT_ROOT']."/habiba/adminproject/image/";
move_uploaded_file($temp,$destination.$image);

$display = "SELECT  `ProductId`,`ProductTitle`, `Price`, `ProductsSpecification`, `ProductDetails`, `image` FROM `product`";
$result = $conn->query($display);

$conn->close();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include_once('partials/head.php');?>
</head>
 <body>
 <body class="sb-nav-fixed">
<?php 
include_once('partials/head.php'); 
include_once('partials/header.php'); 
include_once('partials/sidebar.php');
?>
<div id="layoutSidenav_content">
<main>
    <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                DataTable Example
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                           
                                            <th>Product Title</th>
                                            <th>Price</th>
                                            <th>Products Specification</th>
                                            <th>Product details</th>
                                            <th>Image</th>
                                            <th><i class="fas fa-pen"></th>
                                            <th><i class="fas fa-trash-can"></th>
                                             
                                            
                                           
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        
                                    <tr>
                                            
                                            <th>Product Title</th>
                                            <th>Price</th>
                                            <th>Products Specification</th>
                                            <th>Product details</th>
                                            <th>Image</th>
                                            <th><i class="fas fa-pen"></th>
                                            <th><i class="fas fa-trash-can"></th>
                                            
                                           
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <tr>
                                    <?php
        while($row = $result->fetch_assoc()) { 
        ?>
        <tr>
        <td><?= $row["ProductTitle"];?></td>
        <td><?= $row["Price"];?></td>
        <td><?= $row["ProductsSpecification"];?></td>
        <td><?= $row["ProductDetails"];?></td> 
        <td><img src="image/<?= $row['image'];?>" alt="" style="width:100px; height:100px"></td>
        <td><a href='update.php?ProductId=<?= $row['ProductId']?>'><i class="fas fa-pen"></a></td>
        <td><a href='delete.php?ProductId=<?=  $row['ProductId']?>'><i class="fas fa-trash-can"></a></td>
                                            
                                           
        </tr>
        <?php
}
?>
        </tbody>
    </table>
    </div>
</div>
 </div>
</main>
</div>
                                 

<?php include_once('partials/footer.php');?>
    
</body>
</html>
