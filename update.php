<?php
include_once('db/conn.php');

$ProductId = $_GET['ProductId'];
//echo $ProductId;
$display = "SELECT * FROM `product` WHERE `ProductId`=$ProductId";
$result = $conn->query($display);

$row = $result->fetch_assoc();


if(isset($_POST['update'])){
    $productTitle = $_POST['productTitle']?? '';
    $price = $_POST['price']?? '';
    $productsSpecification = $_POST['productsSpecification']?? '';
    $productDetails = $_POST['productDetails']?? '';
    $product_image = $_FILES['image']['name']?? '';

    $sql = "UPDATE `product` SET `ProductTitle`='$productTitle',`Price`='$price',`ProductsSpecification`='$productsSpecification',`ProductDetails`='$productDetails',`image`='$product_image' WHERE `ProductId`=$ProductId";

    if ($conn->query($sql) === TRUE) {
        
    $temp = $_FILES['image']['tmp_name']?? '';
    $destination="image/".$product_image;
    move_uploaded_file($temp,$destination);
    header('Location:dataTable.php');

  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

  
    
}





?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>productUpdateForm</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
    

    
    <div class="row ">
      <div class="col-lg-7 mx-auto">
        <div class="card mt-2 mx-auto p-4 bg-light">
            <div class="card-body bg-light">
       
            <div class = "container">
            <form action="" method="POST" enctype="multipart/form-data">

            

            <div class="controls">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ProductTitle">Product Title*</label>
                            <input id="ProductTitle" type="text" name="productTitle" class="form-control" value="<?=$row['ProductTitle']?>" required="required" data-error="Product Title is required.">
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Price">Price *</label>
                            <input id="Price" type="text" name="price" class="form-control" value="<?=$row['Price']?>" required="required" data-error="Price is required.">
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ProductsSpecification">Products Specification *</label>
                            <textarea id="ProductsSpecification" name="productsSpecification" class="form-control"  rows="4" required="required" data-error="Product Specification is important." ><?=$row['ProductsSpecification']?></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ProductDetails">Product details </label>
                            <textarea id="ProductDetails" name="productDetails" class="form-control"  rows="4"><?=$row['ProductDetails']?></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <img src="image/<?= $row['image'];?>" alt="" style="width:100px; height:100px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Upload Product Image </label>
                            <input type="file" name ="image" class="form-control" id="image">

                        </div>  
                    </div> 

                </div>
                <br>
              
                    <div class="col-md-12">
                        
                        <input type="submit" class="btn btn-success btn-send  pt-2 btn-block
                            "value="Save" name="update" >
                    </div>
               
          
                


        </div>
         </form>
        </div>
            </div>


    </div>
        <!-- /.8 -->

    </div>
    <!-- /.row-->

</div>
</div>


</body>
</html>


