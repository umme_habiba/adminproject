<!DOCTYPE html>
<html lang="en">
<head>
  <title>productEntryForm</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
        <div class=" text-center mt-5 ">

            <h1> Enter New Product</h1>
                
            
        </div>

    
    <div class="row ">
      <div class="col-lg-7 mx-auto">
        <div class="card mt-2 mx-auto p-4 bg-light">
            <div class="card-body bg-light">
       
            <div class = "container">
                             <form action="dataTable.php" method="POST" enctype="multipart/form-data" id="contact-form" role="form">

            

            <div class="controls">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ProductTitle">Product Title*</label>
                            <input id="ProductTitle" type="text" name="ProductTitle" class="form-control" placeholder="Please enter your Product Title *" required="required" data-error="Product Title is required.">
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Price">Price *</label>
                            <input id="Price" type="text" name="Price" class="form-control" placeholder="Please enter product price *" required="required" data-error="Price is required.">
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ProductsSpecification">Products Specification *</label>
                            <textarea id="ProductsSpecification" name="ProductsSpecification" class="form-control" placeholder="Product Specification is important." rows="4"></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ProductDetails">Product details </label>
                            <textarea id="ProductDetails" name="ProductDetails" class="form-control" placeholder="Write your product details here." rows="4"></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Upload Product Image </label>
                            <input type="file" name ="image" class="form-control" id="image">
                        </div>  
                    </div> 

                </div>
                <br>
              
                    <div class="d-grid gap-2">
                        
                        <input type="submit" class="btn btn-primary btn-send  pt-2 btn-block 
                            " name="insert" value="Save" >
                            
                    </div>
               
          
                


        </div>
         </form>
        </div>
            </div>


    </div>
        <!-- /.8 -->

    </div>
    <!-- /.row-->

</div>
</div>


</body>
</html>
