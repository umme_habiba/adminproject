<!DOCTYPE html>
<html lang="en">
<head>
  <title>productEntryForm</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
        <div class=" text-center mt-5 ">

            <h5> Enter New Slider</h5>
                
            
        </div>

    
    <div class="row ">
      <div class="col-lg-7 mx-auto">
        <div class="card mt-2 mx-auto p-4 bg-light">
            <div class="card-body bg-light">
       
            <div class = "container">
                             <form action="slider.php" method="POST" enctype="multipart/form-data" id="contact-form" role="form">

            

            <div class="controls">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sliderTitle">Slider Title*</label>
                            <input id="sliderTitle" type="text" name="sliderTitle" class="form-control" placeholder="Please enter your slider title *" required="required" data-error="slider title is required.">
                            
                        </div>
                    </div>
                    
               
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="sliderDetails">Slider details </label>
                            <textarea id="sliderDetails" name="sliderDetails" class="form-control" placeholder="Write your slider details here." rows="4"></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Upload slider Image </label>
                            <input type="file" name ="image" class="form-control" id="image">
                        </div>  
                    </div> 

                </div>
                <br>
              
                    <div class="d-grid gap-2">
                        
                        <input type="submit" class="btn btn-primary btn-send  pt-2 btn-block 
                            " name="save" value="Save" >
                            
                    </div>
               
          
                


        </div>
         </form>
        </div>
            </div>


    </div>
        <!-- /.8 -->

    </div>
    <!-- /.row-->

</div>
</div>


</body>
</html>
