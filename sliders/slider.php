<?php
include_once('../db/conn.php');
if(isset($_POST['save'])){
$sliderTitle= $_POST['sliderTitle'];
$sliderDetails= $_POST['sliderDetails'];
$picture=$_FILES['image']?? '';
$image= $picture['name'];

$sql = "INSERT INTO `sliders`(`id`, `sliderTitle`, `slider`, `sliderDetails`, `created_at`, `updated_at`) VALUES  (null,'$sliderTitle','$image','$sliderDetails',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
}
$temp = $picture['tmp_name']?? '';
$destination=$_SERVER['DOCUMENT_ROOT']."/habiba/adminproject/sliders/image/";
move_uploaded_file($temp,$destination.$image);

$display = "SELECT * FROM `sliders` WHERE 1";
$result = $conn->query($display);

$conn->close();


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Admin Dashboard</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="../css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="index.html">The Gadget Zone</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">Settings</a></li>
                        <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="#!">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="adminDashboard.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Product</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts1" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-computer"></i></div>
                                Desktop
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts1" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="productList.php">Brand PC</a>
                                    <a class="nav-link" href="productList.php">Gaming PC</a>
                                    <a class="nav-link" href="productList.php">Lenevo</a>
                                    <a class="nav-link" href="productList.php">HP</a>
                                    <a class="nav-link" href="productList.php">DELL</a>
                                 
                                </nav>
                           </div>
                                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-laptop"></i></div>
                                    Laptop
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="productList.php">Gaming Laptop</a>
                                        <a class="nav-link" href="productList.php">Premium ultrabook</a>
                                        
                                       
                                    </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts3" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas  fa-keyboard"></i></div>
                                Component
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts3" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="productList.php">Processor</a>
                                    <a class="nav-link" href="productList.php">CPU Cooler</a>
                                    <a class="nav-link" href="productList.php">Motherboard</a>
                                    <a class="nav-link" href="productList.php">Graphics Card</a>
                                    <a class="nav-link" href="productList.php">RAM (Desktop)</a>
                                    <a class="nav-link" href="productList.php">RAM (Laptop)</a>
                                    <a class="nav-link" href="productList.php">Power Supply</a>
                                    <a class="nav-link" href="productList.php">Hard Disk Drive</a>
                                    <a class="nav-link" href="productList.php">SSD</a>
                                    <a class="nav-link" href="productList.php">Casing</a>
                                    <a class="nav-link" href="productList.php">Optical Disk Drive</a>
                                    
                                    
                                </nav>
                        </div>

                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts4" aria-expanded="false" aria-controls="collapseLayouts">
                            <div class="sb-nav-link-icon"><i class="fas fa-desktop"></i></div>
                            Monitor
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseLayouts4" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="productList.php">BenQ</a>
                                <a class="nav-link" href="productList.php">LG</a>
                                <a class="nav-link" href="productList.php">ASUS</a>
                                <a class="nav-link" href="productList.php">MSI</a>
                                <a class="nav-link" href="productList.php">HP</a>
                                <a class="nav-link" href="productList.php">Dell</a>
                                <a class="nav-link" href="productList.php">Samsung</a>
                                <a class="nav-link" href="productList.php">Viewsonic</a>
                                <a class="nav-link" href="productList.php">Lenevo</a>
                                <a class="nav-link" href="productList.php">PHILIPS</a>
                                <a class="nav-link" href="productList.php">Walton</a>
                               
                                
                            </nav>
                    </div>
                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts5" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-computer-speaker"></i></div>
                        Accessories
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseLayouts5" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="productList.php">Keyboard</a>
                            <a class="nav-link" href="productList.php">Mouse</a>
                            <a class="nav-link" href="productList.php">Headphones</a>
                            <a class="nav-link" href="productList.php">Mouse Pad</a>
                            <a class="nav-link" href="productList.php">Speaker</a>
                            <a class="nav-link" href="productList.php">Webcam</a>
                            <a class="nav-link" href="productList.php">Microphone</a>
                            <a class="nav-link" href="productList.php">Presenter</a>
                            <a class="nav-link" href="productList.php">Memory Card</a>
                            <a class="nav-link" href="productList.php">Sound Card</a>
                            <a class="nav-link" href="productList.php">Pen Drive</a>
                            <a class="nav-link" href="productList.php">Thermal Paste</a>
                           <a class="nav-link" href="productList.php">UPS</a>
                            
                           
                        </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts6" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Gaming
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts6" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="productList.php">XBOX</a>
                        <a class="nav-link" href="productList.php">Play Station/a>
                        <a class="nav-link" href="productList.php">Gaming Chair</a>
                        <a class="nav-link" href="productList.php">Gaming Mouse</a>
                        <a class="nav-link" href="productList.php">Gaming Keyboard</a>
                        
                        
                    </nav>
            </div>
            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts7" aria-expanded="false" aria-controls="collapseLayouts">
                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                Gadget
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse" id="collapseLayouts7" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="productList.php">Smart Watch</a>
                    <a class="nav-link" href="productList.php">Smart Band</a>
                    <a class="nav-link" href="productList.php">Ear Phones</a>
                    <a class="nav-link" href="productList.php">TV BOX</a>
                    <a class="nav-link" href="productList.php">Power Bank</a>
                    <a class="nav-link" href="productList.php">Drones</a>
                    <a class="nav-link" href="productList.php">Calculator</a>
                    <a class="nav-link" href="productList.php">Blower</a>
                    <a class="nav-link" href="productList.php">Mobile Phone Accesories</a>

                </nav>
        </div>
      
                 <div class="sb-sidenav-menu-heading">Management</div>
                            <a class="nav-link" href="#">
                                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Users
                            </a>

                            
                            <a class="nav-link" href="">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Tables
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Admin
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                        <main>
    <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                DataTable Example
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                           
                                            <th>Slider Title</th>
                                            <th>Slider</th>
                                            <th>Slider details</th>
                                            <th><i class="fas fa-pen"></th>
                                            <th><i class="fas fa-trash-can"></th>
                                             
                                            
                                           
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                    <tr>
                                    <?php
        while($row = $result->fetch_assoc()) { 
        ?>
        <tr>
        <td><?= $row['sliderTitle'];?></td>   
        <td><img src="image/<?= $row['slider'];?>" alt="" style="width:100px; height:100px"></td>
        <td><?= $row['sliderDetails'];?></td>
        <td><i class="fas fa-pen"></a></td>
        <td><i class="fas fa-trash-can"></a></td>
                                            
                                           
        </tr>
        <?php
}
?>
        </tbody>
    </table>
    </div>
</div>
 </div>
</main>
                  
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; The Gadget Zone </div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/dataTable.js"></script>
    </body>
</html>



